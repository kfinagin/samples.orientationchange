﻿namespace PortraitCaliburnTest.ViewModels
{
    using Caliburn.Micro;

    public class TestPageViewModel : Screen, ICloneable
    {
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }

    public interface ICloneable
    {
        object Clone();
    }
}
