﻿namespace PortraitCaliburnTest
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Windows.ApplicationModel.Activation;
    using Windows.UI.Xaml.Controls;
    using Caliburn.Micro;
    using PortraitCaliburnTest.ViewModels;
    using PortraitCaliburnTest.ViewModels.Shell;

    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : CaliburnApplication
    {
        private WinRTContainer container;

        public App()
        {
            this.InitializeComponent();
        }

        protected override void Configure()
        {
            this.ConfigureTypeMappings();

            this.container = new WinRTContainer();
            this.container.RegisterWinRTServices();

            this.container.PerRequest<ShellViewModel>();

        }

        private void ConfigureTypeMappings()
        {
            ViewLocator.LocateForModelType = (viewModelType, visualParent, context) =>
            {
                string primaryViewTypeName = string.Empty;
                string secondaryViewTypeName = string.Empty;

                if (viewModelType.FullName.Contains("Shell"))
                {
                    primaryViewTypeName = viewModelType.FullName
                        .Replace("ViewModels", "Views")
                        .Replace("Model", string.Empty);
                }
                else if ((string)context == "Portrait")
                {
                    // first search in portrait, then search in landscape
                    primaryViewTypeName = viewModelType.FullName
                        .Replace("ViewModels", "Views.Portrait")
                        .Replace("Model", string.Empty);
                    secondaryViewTypeName = viewModelType.FullName
                        .Replace("ViewModels", "Views.Landscape")
                        .Replace("Model", string.Empty);
                }
                else
                {
                    // first search in landscape, then search in portrait
                    primaryViewTypeName = viewModelType.FullName
                        .Replace("ViewModels", "Views.Landscape")
                        .Replace("Model", string.Empty);
                    secondaryViewTypeName = viewModelType.FullName
                        .Replace("ViewModels", "Views.Portrait")
                        .Replace("Model", string.Empty);
                }

                var viewType = AssemblySource.FindTypeByNames(new[] { primaryViewTypeName, secondaryViewTypeName });
                var view = ViewLocator.GetOrCreateViewType(viewType);

                return view;
            };
        }

        protected override object GetInstance(Type service, string key)
        {
            var instance = this.container.GetInstance(service, key);
            if (instance != null) return instance;

            throw new Exception("Could not locate any instances");
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return this.container.GetAllInstances(service);
        }

        protected override void BuildUp(object instance)
        {
            this.container.BuildUp(instance);
        }

        protected override void PrepareViewFirst(Frame rootFrame)
        {
            this.container.RegisterNavigationService(rootFrame);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            this.DisplayRootViewFor<ShellViewModel>();
        }
    }
}
