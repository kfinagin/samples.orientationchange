﻿namespace PortraitCaliburnTest.ViewModels.Shell
{
    using System;
    using System.IO;
    using Windows.UI.Xaml;
    using Caliburn.Micro;
    using PropertyChanged;

    /// <summary>
    /// The shell view model.
    /// </summary>
    [ImplementPropertyChanged]
    public class ShellViewModel : Conductor<IScreen>.Collection.AllActive
    {
        private PageLayout currentPageLayout;

        public ShellViewModel()
        {
            ActiveItemViewModel = new TestPageViewModel();
            SecondActiveItemViewModel = new TestPage2ViewModel();
        }

        public Screen ActiveItemViewModel { get; set; }

        public Screen SecondActiveItemViewModel { get; set; }

        public void OnSizeChanged()
        {
            var defaultLocateForModelType = ViewLocator.LocateForModelType;

            ViewLocator.LocateForModelType = (viewModelType, visualParent, context) =>
            {
                var viewTypeName = viewModelType.FullName.Replace("Model", string.Empty);
                if ((string)context == "Portrait")
                {
                    
                }
                if ((string) context == "Landscape")
                {
                    
                }
            };

            ReconfigureMappings();

        }

        public string Context { get; set; }

        private void ReconfigureMappings()
        {
            switch (CurrentPageLayout)
            {
                case PageLayout.Landscape:
                    ConfigureLandscape();
                    Context = "Landscape";
                    break;
                case PageLayout.Portrait:
                    ConfigurePortrait();
                    Context = "Portrait";
                    break;
                case PageLayout.Minimal:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void ConfigureLandscape()
        {
            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels.Shell", "PortraitCaliburnTest.Views.Shell");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Shell", "PortraitCaliburnTest.ViewModels.Shell");

            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels", "PortraitCaliburnTest.Views.Portrait");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Portrait", "PortraitCaliburnTest.ViewModels");

            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels", "PortraitCaliburnTest.Views.Landscape");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Landscape", "PortraitCaliburnTest.ViewModels");
        }

        private void ConfigurePortrait()
        {
            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels.Shell", "PortraitCaliburnTest.Views.Shell");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Shell", "PortraitCaliburnTest.ViewModels.Shell");

            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels", "PortraitCaliburnTest.Views.Landscape");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Landscape", "PortraitCaliburnTest.ViewModels");

            ViewLocator.AddSubNamespaceMapping("PortraitCaliburnTest.ViewModels", "PortraitCaliburnTest.Views.Portrait");
            ViewModelLocator.AddSubNamespaceMapping("PortraitCaliburnTest.Views.Portrait", "PortraitCaliburnTest.ViewModels");
        }

        public PageLayout CurrentPageLayout
        {
            get
            {
                return this.currentPageLayout;
            }

            set
            {
                if (value == this.currentPageLayout) return;
                this.currentPageLayout = value;
                NotifyOfPropertyChange(() => CurrentPageLayout);
                OnSizeChanged();
            }
        }
    }
}
