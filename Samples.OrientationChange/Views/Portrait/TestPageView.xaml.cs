﻿namespace PortraitCaliburnTest.Views.Portrait
{
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TestPageView : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TestPageView"/> class.
        /// </summary>
        public TestPageView()
        {
            this.InitializeComponent();
        }
    }
}
