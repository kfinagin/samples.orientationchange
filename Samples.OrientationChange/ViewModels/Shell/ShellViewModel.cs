﻿namespace PortraitCaliburnTest.ViewModels.Shell
{
    using System;
    using System.IO;
    using Windows.UI.Xaml;
    using Caliburn.Micro;
    using PropertyChanged;

    /// <summary>
    /// The shell view model.
    /// </summary>
    [ImplementPropertyChanged]
    public class ShellViewModel : Conductor<IScreen>.Collection.AllActive
    {
        private PageLayout currentPageLayout;

        public ShellViewModel()
        {
            ActiveItemViewModel = new TestPageViewModel();
            SecondActiveItemViewModel = new TestPage2ViewModel();
        }

        public Screen ActiveItemViewModel { get; set; }

        public Screen SecondActiveItemViewModel { get; set; }



        private string context;

        public string Context
        {
            get
            {
                return this.context;
            }

            set
            {
                if (value == this.context) return;
                this.context = value;
                NotifyOfPropertyChange(() => Context);
            }
        }

        public PageLayout CurrentPageLayout
        {
            get
            {
                return this.currentPageLayout;
            }

            set
            {
                if (value == this.currentPageLayout) return;
                this.currentPageLayout = value;
                NotifyOfPropertyChange(() => CurrentPageLayout);
                OnSizeChanged();
            }
        }

        private void OnSizeChanged()
        {
            switch (CurrentPageLayout)
            {
                case PageLayout.Landscape:
                    Context = "Landscape";
                    break;
                case PageLayout.Portrait:
                    Context = "Portrait";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
