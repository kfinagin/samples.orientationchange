﻿namespace PortraitCaliburnTest.Views.Shell
{
    using PortraitCaliburnTest.Views.Controls;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ShellView : LayoutInformingPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ShellView"/> class.
        /// </summary>
        public ShellView()
        {
            this.InitializeComponent();
        }
    }
}
