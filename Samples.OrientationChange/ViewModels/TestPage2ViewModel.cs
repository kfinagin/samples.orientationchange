﻿namespace PortraitCaliburnTest.ViewModels
{
    using Caliburn.Micro;

    /// <summary>
    /// The test page 2 view model.
    /// </summary>
    public class TestPage2ViewModel : Screen, ICloneable
    {
        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
