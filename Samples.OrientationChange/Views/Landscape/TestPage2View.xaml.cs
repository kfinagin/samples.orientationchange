﻿// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PortraitCaliburnTest.Views.Landscape
{
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TestPage2View : Page
    {
        public TestPage2View()
        {
            this.InitializeComponent();
        }
    }
}
