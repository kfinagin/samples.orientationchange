﻿namespace PortraitCaliburnTest.Views.Controls
{
    using System;
    using System.ComponentModel;
    using PortraitCaliburnTest.ViewModels;
    using Windows.UI.Xaml;
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// The layout informing page.
    /// </summary>
    public class LayoutInformingPage : Page
    {
        public LayoutInformingPage()
        {
            CurrentPageLayout = PageLayout.Landscape;
            this.SizeChanged += OnSizeChanged;
        }

        public static readonly DependencyProperty CurrentPageLayoutProperty = DependencyProperty.Register(
            "CurrentPageLayout", typeof(PageLayout), typeof(LayoutInformingPage), new PropertyMetadata(default(PageLayout)));

        // property to detect screen orientation change
        public PageLayout CurrentPageLayout
        {
            get { return (PageLayout)GetValue(CurrentPageLayoutProperty); }
            set { SetValueDp(CurrentPageLayoutProperty, value); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetValueDp(
            DependencyProperty property, 
            object value, 
            [System.Runtime.CompilerServices.CallerMemberName] string p = null)
        {
            SetValue(property, value);
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(p));                
            }
        }

        private void OnSizeChanged(object s, SizeChangedEventArgs e)
        {
            var newPageLayout = DeterminePageLayout(e.NewSize.Width, e.NewSize.Height);
            CurrentPageLayout = newPageLayout;
        }

        private PageLayout DeterminePageLayout(double width, double height)
        {
            if (width > height) return PageLayout.Landscape;
            return PageLayout.Portrait;
        }
    }
}
